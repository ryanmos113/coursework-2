package ljmu.vets;

import java.io.Serializable;

public class SurgeryMedicine implements Payable, Serializable {
	protected String name;
	protected Integer stock, lowest;
	protected Double surgeryCost;
	// protected List<Notifiable> people = new ArrayList<Notifiable>();

	public SurgeryMedicine(String name, Integer stock, Integer lowest, Double surgeryCost) {
		this.name = name;
		this.stock = stock;
		this.lowest = lowest;
		this.surgeryCost = surgeryCost;
	}

	/*
	public void setStock(Integer i) {
		this.stock = i;

		if (i < lowest) {
			notifyPeople();
		}
	}

	public void addNotifiable(Notifiable n) {
		this.people.add(n);
	}

	private void notifyPeople() {
		for	(Notifiable n : people) {
			n.notify(this.stock + " of " + this.name);
		}
	}
	*/

	@Override
	public Double getSurgeryCost() {
		return this.surgeryCost;
	}

	@Override
	public Double getPublicCost() {
		return -1.0;
	}

	// ToDo : get / set Methods ?
}
