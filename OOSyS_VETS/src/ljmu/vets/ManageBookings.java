package ljmu.vets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ManageBookings { //new class to reduce responsibility of the surgery class

	    private List<Booking> bookings = Collections.synchronizedList(new ArrayList<Booking>());
	    
	    public void createBooking(Booking booking) {
	        // ToDo: Validate
	        this.bookings.add(booking); // creates a new booking
	    }
	    
	    // Todo: getters and setters
	}