package ljmu.vets;

public interface InvoiceExporter {
    void exportInvoice(Invoice invoice, String path); //exports the invoice which has be formatted by another class
}
