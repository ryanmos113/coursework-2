package ljmu.vets;

import java.time.LocalDate; 
import java.time.format.DateTimeFormatter;

public class Cat extends Pet {
    public Cat(String name, LocalDate regDate) {
        super(name, regDate);
    }

    @Override
    public String getType() { // returns pet type as a string to the parent class
        return "Cat";
    }

    @Override
    public String toString() {
        return getType() + " >> " + //code simplified by using getType instead of return this.getClass().getSimpleName()
                this.name + " " +
                this.regDate.format(DateTimeFormatter.ofPattern("dd MMM yy"));
    }

    // ToDo : get / set Methods ?
}
