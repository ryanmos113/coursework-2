package ljmu.vets;

import java.util.ArrayList;
import java.util.List;

public class ManagePets { // new class to reduce responsibility of surgery class
	    private static List<Pet> pets = new ArrayList<Pet>();
	    
	    public static void createPet(Pet pet) {
	        // ToDo: Validate
	        ManagePets.pets.add(pet); //create a new pet and add it to the list
	    }

	    public static Pet fetchPet(String name) {
	    	for (int i = 0; i < pets.size(); i++) {
	    		String fetchedPet = (pets.get(i)).getName();
	            if (fetchedPet.equals(name)) {
	                return pets.get(i); //searches the list for a pet by name
	            }
	        }

	        return null;
	    }
	}