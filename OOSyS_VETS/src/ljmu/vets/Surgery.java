package ljmu.vets;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

 public class Surgery implements Serializable {

	private static final long serialVersionUID = 1L;
			private String surgery; 
		    private List<Booking> bookings = Collections.synchronizedList(new ArrayList<Booking>());

		    public Surgery(String surgery) {
		        this.surgery = surgery; //initialises surgery
		    }

		    @Override
		    public String toString() {
		        return this.getClass().getSimpleName() + " >> " + this.surgery; 
		    } //creates a string representation of the object

		    public void createBooking(Booking booking) {
		        // todo validate
		        this.bookings.add(booking); // creates new booking and adds it to the list

		        booking.getPet().makeBooking(booking);
		    }
		    
		    // todo getters and setters needed
		    
		}